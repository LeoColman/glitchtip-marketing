import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SDKDocsIndexComponent } from './sdk-docs-index.component';
import { SDKDocsComponent } from './sdk-docs.component';

const routes: Routes = [
  {
    path: '',
    component: SDKDocsIndexComponent,
  },
  {
    path: ':sdkdocs',
    component: SDKDocsComponent,
  },
  {
    path: '**',
    component: SDKDocsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SDKDocsRoutingModule {}
