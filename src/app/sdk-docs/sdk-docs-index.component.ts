import { Component, ViewEncapsulation } from '@angular/core';
import { ScullyRoutesService } from '@scullyio/ng-lib';
import { map } from 'rxjs/operators';
import { platforms as categorizedPlatforms } from 'glitchtip-frontend/src/app/settings/projects/platform-picker/platforms';

const SDK_DOCS_ROUTE = '/sdkdocs/';
const sdkBlacklist = [
  'all-sdks.md', // Not an actual SDK doc
  'other.md', // Ditto
];

@Component({
  selector: 'app-sdk-docs',
  templateUrl: './sdk-docs-index.component.html',
  styles: [
    `
      .unstyled {
        list-style: none;
        padding: 0;
      }
    `,
  ],
  preserveWhitespaces: true,
  encapsulation: ViewEncapsulation.Emulated,
})
export class SDKDocsIndexComponent {
  categorizedSdkDocRoutes$ = this.scully.available$.pipe(
    map((availableRoutes) => {
      const routesToUse = availableRoutes
        .filter((routeObject) => routeObject.route.startsWith(SDK_DOCS_ROUTE))
        .filter(({ sourceFile }) => sourceFile && !sdkBlacklist.includes(sourceFile));

      return categorizedPlatforms.map((categorizedPlatform) => {
        const integrations = categorizedPlatform.integrations
          .map((integration) => ({
            ...integration,
            route: routesToUse.find(
              ({ sourceFile }) => sourceFile?.slice(0, -3) === integration.id
            )?.route,
          }))
          // In lieu of a proper dedupe
          .filter((integration) => integration.id !== 'cocoa');
        return {
          ...categorizedPlatform,
          integrations,
        };
      });
    })
  );

  constructor(private scully: ScullyRoutesService) {}
}
